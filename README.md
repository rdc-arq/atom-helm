# Atom Helm

## How it works? 

This project deploys: [atom](https://www.accesstomemory.org/pt-br/), elasticsearch and mysql on a kubernetes cluster. In addiction, it creates an admin user on atom. Docker image used on this project can be found [here](https://gitlab.com/rdc-arq/atom). Mysql and elasticsearch was taken from docker hub. 

## How to use?

- helm repo add atom https://gitlab.com/api/v4/projects/37677650/packages/helm/stable
- helm repo update
- helm install -f values.yml atom/atom --generate-name (-n your namespace here, it you don't want to deploy on default)

Obs. You need to pick values.yml and change values as needed. 
